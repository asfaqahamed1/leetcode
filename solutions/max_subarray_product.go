package solutions

func MaxSubArrayProduct() int {
	nums := []int{0, 10, 10, 10, 10, 10, 10, 10, 10, 10, -10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 0}
	maxProduct := nums[0]
	currentLeftProduct := 1
	currentRightProduct := 1

	for i := range nums {
		if currentLeftProduct == 0 {
			currentLeftProduct = 1
		}
		if currentRightProduct == 0 {
			currentRightProduct = 1
		}
		if currentLeftProduct > -1000000000000000000 && currentRightProduct > -1000000000000000000 {
			currentLeftProduct *= nums[i]
			currentRightProduct *= nums[len(nums)-1-i]
			maxProduct = max(currentLeftProduct, currentRightProduct, maxProduct)
		}

	}
	return maxProduct
}

func max(a, b, c int) int {
	if a >= b && a >= c {
		return a
	} else if b >= a && b >= c {
		return b
	}
	return c
}

package solutions

func MinInRotatedSort() int {
	nums := []int{4, 5, 6, 7, 0, 1, 2}
	start := 0
	end := len(nums) - 1
	if start == end {
		return nums[start]
	}
	for start < end {

		mid := (start + end) / 2
		if nums[mid] > nums[start] && nums[mid] > nums[end] {
			start = mid + 1
		} else if nums[mid] < nums[start] && nums[mid] < nums[end] {
			end--
		} else if nums[mid] >= nums[start] && nums[mid] > nums[end] {
			start++
		} else {
			end--
		}
	}
	return nums[start]
}

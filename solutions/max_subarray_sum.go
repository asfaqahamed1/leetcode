package solutions

func MaxSubArraySum() int {

	currentSum := 0
	nums := []int{-2, 1, -3, 4, -1, 2, 1, -5, 4}
	maxSum := nums[0]

	for _, val := range nums {
		if currentSum < 0 {
			currentSum = 0
		}
		currentSum += val
		if currentSum > maxSum {
			maxSum = currentSum
		}
	}
	return maxSum

}

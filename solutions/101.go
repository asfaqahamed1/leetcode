package solutions

import "fmt"

func S101() {
	root := &TreeNode{Val: 1}
	root.Left = &TreeNode{Val: 2}
	root.Right = &TreeNode{Val: 2}
	root.Left.Left = &TreeNode{Val: 3}
	root.Left.Right = &TreeNode{Val: 4}
	root.Right.Left = &TreeNode{Val: 4}
	root.Right.Right = &TreeNode{Val: 3}
	fmt.Println(trying(root.Left, root.Right))
}
func trying(root, root1 *TreeNode) bool {
	if root.Left == nil && root1.Right == nil {
		return true
	}
	if (root == nil && root1 != nil) || (root != nil && root1 == nil) || (root.Val != root1.Val) {
		return false
	}

	return trying(root.Left, root1.Right) && trying(root.Right, root1.Left)

}

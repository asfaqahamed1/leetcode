package solutions

type Tire struct {
	children []*Tire
	terminal bool
}

func createTire() *Tire {
	tire := Tire{children: make([]*Tire, 26), terminal: false}
	for i := range tire.children {
		tire.children[i] = nil
	}
	return &tire
}

var tireNode = createTire()

func insert(key string) {
	currentNode := tireNode
	for _, char := range key {
		index := char - 'a'
		if currentNode.children[index] == nil {
			currentNode.children[index] = createTire()
		}
		currentNode = currentNode.children[index]
	}
	currentNode.terminal = true
}

func countChilderns(t *Tire) (int, int) {
	count := 0
	index := 0
	for i := range t.children {
		if t.children[i] != nil {
			count += 1
			index = i
		}
	}
	return count, index
}

func walkTrie() string {
	prefix := ""
	currentNode := tireNode
	for {
		count, index := countChilderns(currentNode)
		if count != 1 || currentNode.terminal {
			break
		}
		prefix += string(rune(97 + index))
		currentNode = currentNode.children[index]
	}
	return prefix
}
func LongestCommonPrefix(strs []string) string {
	for _, str := range strs {
		insert(str)
	}
	lngPrefix := walkTrie()
	println(lngPrefix)
	return lngPrefix
}

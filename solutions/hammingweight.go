package solutions

func HammingWeight() int {
	n := 128
	res := 0
	for n > 0 {
		res += n % 2
		n = n >> 1
	}
	return res
}

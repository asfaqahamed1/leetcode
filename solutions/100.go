package solutions

import "fmt"

func S100() {
	p := &TreeNode{Val: 1}
	p.Left = &TreeNode{Val: 2}
	q := &TreeNode{Val: 1}
	q.Right = &TreeNode{Val: 2}
	sameTree := true

	fmt.Println(postOrder(p, q, sameTree))
}

func postOrder(p, q *TreeNode, sameTree bool) bool {
	if p == nil && q == nil {
		return true
	}
	if (p == nil && q != nil) || (p != nil && q == nil) || p.Val != q.Val {
		return false
	}
	sameTree = sameTree && postOrder(p.Left, q.Left, sameTree)
	sameTree = sameTree && postOrder(p.Right, q.Right, sameTree)
	return sameTree
}

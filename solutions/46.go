package solutions

import "fmt"

func S46() {
	nums := []int{1, 2, 3}
	res := [][]int{}
	for i := 0; i < len(nums); i++ {
		permutate(i, nums, []int{}, &res)
	}
	fmt.Println(res)
}

func permutate(index int, nums, currentSum []int, res *[][]int) {
	if index >= len(nums) {
		return
	}
	currentSum = append(currentSum, nums[index])
	if len(currentSum) == len(nums) {
		*res = append(*res, currentSum)
		return
	}
	i := 0
	for i < len(nums) {
		if !includes(currentSum, nums[i]) {
			permutate(i, nums, currentSum, res)
		}
		i++
	}
}

func includes(nums []int, val int) bool {
	for i := 0; i < len(nums); i++ {
		if nums[i] == val {
			return true
		}
	}
	return false
}

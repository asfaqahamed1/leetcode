package solutions

func CanJump() {
	nums := []int{1, 1, 2, 2, 0, 1, 1}
	mem := make(map[int]bool)
	print(canjump(nums, 0, false, mem))
}

/* dp*/

func canjump(nums []int, index int, cj bool, mem map[int]bool) bool {
	if index >= len(nums)-1 {
		cj = true
		return cj
	}
	val := nums[index]
	if value, ok := mem[index]; ok {
		return value
	}
	for val > 0 {
		cj = cj || canjump(nums, index+val, cj, mem)
		mem[index] = cj
		val--
	}
	return cj
}

/* greedy algo*/

func canJumpGreedy() {
	
}

package solutions

func ProductExceptSelf(nums []int) []int {
	res := make([]int, len(nums))
	for i := range res {
		res[i] = 1
	}
	fix := 1
	for i := range nums {
		res[i] *= fix
		fix *= nums[i]
	}
	fix = 1
	for j := len(nums) - 1; j >= 0; j-- {
		res[j] *= fix
		fix *= nums[j]
	}
	return res
}

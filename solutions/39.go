package solutions

import "fmt"

func S39() {
	candidates := []int{2, 3, 5}
	target := 8
	rs := []int{}
	ans := [][]int{}
	backTrack(candidates, 0, target, rs, &ans)
	fmt.Println(ans)
}

func backTrack(candidates []int, index, target int, rs []int, ans *[][]int) {
	if target == 0 {
		combination := make([]int, len(rs))
		copy(combination, rs)
		*ans = append(*ans, combination)
	}
	for i := index; i < len(candidates); i++ {
		if candidates[i] <= target {
			rs = append(rs, candidates[i])
			backTrack(candidates, i, target-candidates[i], rs, ans)
			rs = rs[:len(rs)-1]
		}
	}
}

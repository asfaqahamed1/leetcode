package solutions

func NumSubarraysWithSum(nums []int, goal int) int {
	totalSubArray := 0
	for i := 0; i < len(nums); i++ {
		sum := nums[i]
		if sum == goal {
			totalSubArray++
		}
		for j := i + 1; j < len(nums); j++ {
			sum = sum + nums[j]
			if sum == goal {
				totalSubArray++
			} else if sum > goal {
				break
			}
		}
	}
	return totalSubArray
}

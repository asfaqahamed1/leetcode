package solutions

import "fmt"

func ProductExceptSelf2() {
	nums := []int{1, 2, 3, 4}
	res := make([]int, len(nums))
	left := 1
	for i, _ := range nums {
		res[i] = left
		left = left * nums[i]
	}
	left = 1
	for i := len(res) - 1; i >= 0; i-- {
		res[i] = left * res[i]
		left = left * nums[i]
	}

	fmt.Println(res)
}

package solutions

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func S94() {
	binaryTree := &TreeNode{Val: 1}
	binaryTree.Right = &TreeNode{Val: 2}
	binaryTree.Right.Left = &TreeNode{Val: 3}
	arr := []int{}
	inorder(binaryTree, &arr)
	fmt.Println(arr)

}

func inorder(tree *TreeNode, arr *[]int) {
	if tree == nil {
		return
	}
	inorder(tree.Left, arr)
	println(tree.Val)
	*arr = append(*arr, tree.Val)
	inorder(tree.Right, arr)
}

package solutions

func BuyAndSell() int {
	profit := 0
	prices := []int{7, 1, 5, 3, 6, 4}
	min_amount := prices[0]

	for _, value := range prices {
		if value < min_amount {
			min_amount = value
		} else if value-min_amount > profit {
			profit = value - min_amount
		}
	}
	return profit
}

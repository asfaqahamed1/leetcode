package solutions

func LengthOfLastWOrd(str string) (count int) {
	for i := len(str) - 1; i >= 0; i-- {
		if str[i] == ' ' {
			if count != 0 {
				return
			}
		} else {
			count++
		}
	}
	return
}

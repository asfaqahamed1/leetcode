package solutions

import "fmt"

func S66() {
	digits := []int{1, 9, 9, 9}
	n := len(digits)
	for i := n - 1; i >= 0; i-- {
		if digits[i] < 9 {
			digits[i]++
			fmt.Println(digits)
		}
		digits[i] = 0

	}
	digits = append([]int{1}, digits...)
	fmt.Println(digits)
}

package solutions

import "fmt"

func S45() {
	nums := []int{2, 3, 0, 1, 4}
	jumpBack(nums, 0, 0)

}

func jumpBack(nums []int, index, count int) int {
	count++
	max := index

	if index == len(nums) {
		fmt.Println(count, "count")
		return count
	}

	fmt.Println(index, nums[index])

	for i := index; i < nums[index]; i++ {
		if nums[i] > nums[index] {
			max = i
		}
	}

	jumpBackCount := jumpBack(nums, max, count)

	if count < jumpBackCount {
		count = jumpBackCount
	}

	return count
}

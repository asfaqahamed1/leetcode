package solutions

var numMap = map[rune]string{
	'2': "abc",
	'3': "def",
	'4': "ghi",
	'5': "jkl",
	'6': "mno",
	'7': "pqrs",
	'8': "tuv",
	'9': "wxyz",
}

func LetterCombinations() []string {
	digits := "23"
	if digits == "" {
		return []string{}
	}
	combos := letterComb(0, digits, "")
	for _, v := range combos {
		println(v)
	}
	return []string{}
}

func letterComb(index int, digit, comb string) []string {
	combos := []string{}
	if index < len(digit) {
		for _, v := range numMap[rune(digit[index])] {
			combos = append(combos, letterComb(index+1, digit, comb+string(v))...)
		}
	} else {
		if comb != "" {
			combos = append(combos, comb)
		}
		return combos
	}
	return combos
}

package solutions

import "log"

func RemoveElement(nums []int, val int) int {
	i := 0
	for i < len(nums) {
		if val == nums[i] {
			nums = append(nums[:i], nums[i+1:]...)
		} else {
			i++
		}
	}
	log.Printf("%v", nums)
	return len(nums)
}

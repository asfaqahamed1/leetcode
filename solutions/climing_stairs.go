package solutions

func ClimingStairs() int {
	memo := make(map[int]int)
	return climbStairs(45, 0, memo)
}

func climbStairs(n int, currentSteps int, memo map[int]int) int {
	if val, ok := memo[currentSteps]; ok {
		return val
	}
	if currentSteps > n {
		return 0
	}
	if currentSteps == n {
		return 1
	}
	total := 0
	total += climbStairs(n, currentSteps+1, memo)
	total += climbStairs(n, currentSteps+2, memo)
	memo[currentSteps] = total
	return total
}

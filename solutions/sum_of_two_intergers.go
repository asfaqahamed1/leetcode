package solutions

func SumOfTwoIntegers() int {
	a := 2
	b := 3
	for b != 0 {
		carry := a & b
		a = a ^ b
		b = carry << 1
	}
	return a
}

package solutions

func RemoveDuplicate(nums []int) int {
	index := 0
	for index < len(nums)-1 {
		if nums[index] == nums[index+1] {
			nums = append(nums[:index], nums[index+1:]...)
		} else {
			index = index + 1
		}
	}
	return len(nums)
}

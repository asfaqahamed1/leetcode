package solutions

import (
	"sort"
)

func ThreeSum() [][]int {
	var result [][]int
	nums := []int{1, -1, -1, 0}
	sort.Ints(nums)

	for i := 0; i < len(nums)-2; i++ {
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}
		target, start, end := -(nums[i]), i+1, len(nums)-1
		for start < end {
			sum := nums[start] + nums[end]
			if target == sum {
				result = append(result, []int{nums[i], nums[start], nums[end]})
				start++
				end--
				for start < end && nums[start] == nums[start-1] {
					start++
				}
				for start < end && nums[end] == nums[end+1] {
					end--
				}
			} else if sum < target {
				start++
			} else if sum > target {
				end--
			}
		}
	}
	return result
}

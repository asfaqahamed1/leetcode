package solutions

func Palindrome(x int) bool {
	num := x
	reversedNum := 0
	if num >= 0 {
		for num > 0 {
			lastDigit := num % 10
			reversedNum = reversedNum*10 + lastDigit
			num = num / 10
		}
		return x == reversedNum
	}
	return false
}

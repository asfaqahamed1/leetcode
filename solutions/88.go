package solutions

import "fmt"

func S88() {
	bruteForceS88()
}

func bruteForceS88() {
	nums1 := []int{0}
	m := 0
	nums2 := []int{1}
	n := 1

	i := 0
	j := m + 0
	for i < n {
		nums1[j] = nums2[i]
		i++
		j++
	}
	fmt.Println(nums1)
	quickSortS88(nums1, 0, len(nums1)-1)
	fmt.Println(nums1)
}

func quickSortS88(arr []int, low, high int) {
	if low < high {
		partitionIndex := partitionS88(arr, low, high)
		quickSortS88(arr, low, partitionIndex-1)
		quickSortS88(arr, partitionIndex+1, high)
	}
}

func partitionS88(arr []int, low, high int) int {
	p := arr[low]
	i := low
	j := high

	for i < j {
		for arr[i] <= p && i < high {
			i++
		}

		for arr[j] > p && j > low {
			j--
		}
		if i < j {
			temp := arr[i]
			arr[i] = arr[j]
			arr[j] = temp
		}
	}
	temp := arr[j]
	arr[j] = arr[low]
	arr[low] = temp
	return j
}

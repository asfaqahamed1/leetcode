package solutions

func TwoSum() []int {
	nums := []int{3, 2, 4}
	target := 6
	sumSlice := make([]int, 2)
Loop:
	for i, _ := range nums {
		for j := 0; j < len(nums); j++ {
			if nums[i]+nums[j] == target && i != j {
				sumSlice[0] = i
				sumSlice[1] = j
				break Loop
			}
		}
	}
	return sumSlice
}

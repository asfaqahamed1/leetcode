package solutions

type Stack []rune

var content = map[rune]rune{
	'(': ')',
	'{': '}',
	'[': ']',
}

func (s *Stack) push(char rune) {
	*s = append(*s, char)
}

func (s *Stack) isEmpty() bool {
	return len(*s) == 0
}

func (s *Stack) pop() rune {
	if s.isEmpty() {
		return 0
	}
	index := len(*s) - 1
	element := (*s)[index]
	*s = (*s)[:index]
	return element
}

func (s *Stack) peek() rune {
	if s.isEmpty() {
		return 0
	}
	index := len(*s) - 1
	element := (*s)[index]
	return element
}

func ValidParentheses() bool {
	s := "([])"
	var stack Stack
	for _, val := range s {
		if _, ok := content[val]; ok {
			stack.push(val)
		} else if content[stack.peek()] != val {
			return false
		} else {
			stack.pop()
		}
	}
	return len(stack) == 0
}

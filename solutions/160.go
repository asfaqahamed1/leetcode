package solutions

func S160(headA, headB *ListNode) *ListNode {
	listNo := &ListNode{}

	for headA != nil && headB != nil {
		if headA.Val == headB.Val {
			insertVal(headA.Val, listNo)
		}
	}

	return headA
}

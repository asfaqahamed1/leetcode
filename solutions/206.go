package solutions

import (
	"fmt"
)

type linkedList struct {
	val  int
	next *linkedList
}

func insetLLValue(val int, head *linkedList) {
	newList := head
	if newList.val == 0 && newList.next == nil {
		newList.val = val
		return
	}
	for newList.next != nil {
		newList = newList.next
	}
	newList.next = &linkedList{val: val}
}

func reverse(head *linkedList) *linkedList {
	if head == nil || head.next == nil {
		return head
	}
	newHead := reverse(head.next)
	front := head.next
	front.next = head
	fmt.Println(head)
	head.next = nil
	return newHead
}

func S206() {
	head := &linkedList{}
	insetLLValue(1, head)
	insetLLValue(2, head)
	insetLLValue(3, head)
	insetLLValue(4, head)
	insetLLValue(5, head)
	newHead := reverse(head)
	fmt.Println(newHead)
}

package solutions

func DecodedStringAtIndex(s string, k int) string {
	length := 0
	stirng_length := 0
	for _, value := range s {
		if value >= '0' && value <= '9' {
			length *= int(value - '0')
		} else {
			length++
		}
		stirng_length++
	}

	for i := stirng_length - 1; i >= 0; i-- {
		if s[i] >= '0' && s[i] <= '9' {
			length /= int(s[i] - '0')
			k %= length
		} else {
			if k == 0 || k == length {
				return string(s[i])
			}
			length--
		}
	}
	return ""

}

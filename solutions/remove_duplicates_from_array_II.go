package solutions

import "fmt"

func RemoveDuplicateII() int {
	nums := []int{0, 0, 1, 1, 1, 1, 2, 3, 3}
	occurence := 0
	index := 0
	currentValue := nums[0]
	for index < len(nums)-1 {
		occurence++
		fmt.Println(occurence)
		if occurence > 2 {
			fmt.Println(index, occurence)
			fmt.Println(nums[:index])
			nums = append(nums[:index], nums[index+1:]...)
		}
		if nums[index+1] != currentValue {
			currentValue = nums[index+1]
			occurence = 0
		}
		index++
	}
	fmt.Println(nums)
	return len(nums)
}

package solutions

import "fmt"

var maping = map[string]int{
	"I":  1,
	"V":  5,
	"X":  10,
	"L":  50,
	"C":  100,
	"D":  500,
	"M":  1000,
	"IX": 9,
	"XC": 90,
	"CM": 900,
	"IV": 4,
	"XL": 40,
	"CD": 400,
}

func RomanToInterger(romanNum string) int {
	engNumber := 0
	i := 0
	for i < len(romanNum) {
		if i == len(romanNum)-1 {
			engNumber += maping[string(romanNum[i])]
			break
		}

		splCase := fmt.Sprintf("%c%c", romanNum[i], romanNum[i+1])
		if value, found := maping[string(splCase)]; found {
			engNumber += value
			i += 2
		} else {
			engNumber += maping[string(romanNum[i])]
			i += 1
		}
	}
	return engNumber
}

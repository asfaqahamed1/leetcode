package solutions

import "slices"

func GenerateParanthesis(n int) []string {
	var result = []string{"()"}
	result1 := generateParanthesis(n, 2, result)
	print("[")
	for _, v := range result1 {
		print(v + " ,")
	}
	print("]")
	return result1
}

func generateParanthesis(n, index int, bt []string) []string {
	if index > n {
		return bt
	}
	var temp = []string{}
	for _, v := range bt {
		he := "(" + v + ")"
		if is := slices.Contains(temp, he); !is {
			temp = append(temp, he)
		}
	}
	for _, v := range bt {
		he := v + "()"
		if is := slices.Contains(temp, he); !is {
			temp = append(temp, he)
		}
	}
	for _, v := range bt {
		he := "()" + v
		if is := slices.Contains(temp, he); !is {
			temp = append(temp, he)
		}

	}
	bt = temp
	return generateParanthesis(n, index+1, bt)
}

package solutions

func CountBits() []int {
	n := 5
	ans := make([]int, n+1)
	ans[0] = 0
	ans[1] = 1
	for i := 2; i <= n; i++ {
		ans[i] = ans[i/2] + i&1
	}
	return ans
}

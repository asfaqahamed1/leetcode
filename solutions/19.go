package solutions

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func insertVal(val int, listNode *ListNode) {
	head := listNode
	if head.Val == 0 && head.Next == nil {
		head.Val = val
		return
	}
	for head.Next != nil {
		head = head.Next
	}
	head.Next = &ListNode{Val: val}
}

func removeNthFromEnd(head *ListNode, n int) {
	loopedIndex := 0
	temp := head
	for temp.Next != nil {
		if loopedIndex >= n {
			temp.Val = temp.Next.Val
		}
		temp = temp.Next
		loopedIndex++
	}
	temp.Next = nil
}

func RemoveNode() {
	head := &ListNode{}
	// reveresedList := &ListNode{}
	insertVal(1, head)
	insertVal(2, head)
	insertVal(3, head)
	insertVal(4, head)
	insertVal(5, head)
	removeNthFromEnd(head, 2)
	fmt.Println(head.Next.Next.Next)
}

package solutions

type stack []rune

func (stck stack) isEmpty() bool {
	return len(stck) < 1
}
func (stck stack) peek() rune {
	if stck.isEmpty() {
		return 0
	}
	return stck[len(stck)-1]
}

func (stck *stack) push(val rune) {
	*stck = append(*stck, val)
}
func (stck *stack) pop() rune {
	element := (*stck)[len(*stck)-1]
	*stck = (*stck)[:len(*stck)-1]
	return element
}
func S20() bool {
	s := ""
	paranthesisStack := make(stack, 0)
	for _, v := range s {
		if v == ')' || v == '}' || v == ']' {
			if v == ')' {
				if paranthesisStack.peek() != '(' {
					return false
				}
			} else if v == '}' {
				if paranthesisStack.peek() != '{' {
					return false
				}
			} else if v == ']' {
				if paranthesisStack.peek() != '[' {
					return false
				}
			}
			paranthesisStack.pop()
		} else {
			paranthesisStack.push(v)
		}
	}
	return true
}

package solutions

import "sort"

func ContainsDuplicate() bool {
	nums := []int{1, 2, 3, 4}
	sort.Ints(nums)
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] == nums[i+1] {
			return true
		}
	}
	return false
}

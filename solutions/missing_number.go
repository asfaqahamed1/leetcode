package solutions

func MissingNumber() int {
	n := []int{9, 6, 4, 2, 3, 5, 7, 0, 1}
	summation := len(n) * (len(n) + 1) / 2
	sum := 0
	for _, v := range n {
		sum += v
	}
	return summation - sum
}

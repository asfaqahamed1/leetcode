package solutions

type LinkedListNode struct {
	Val  int
	Next *LinkedListNode
}

func (l *LinkedListNode) insert(val int) {
	newNode := LinkedListNode{Val: val}
	if l.Val == 0 && l.Next == nil {
		*l = newNode
		return
	}
	head := l
	for head.Next != nil {
		head = head.Next
	}
	head.Next = &newNode
}

func (l1 *LinkedListNode) merge(l2 *LinkedListNode) *LinkedListNode {
	// merge the two LinkedList
	head := &LinkedListNode{}
	tail := head
	for l1 != nil && l2 != nil {
		if l1.Val <= l2.Val {
			tail.Next = l1
			l1 = l1.Next
		} else {
			tail.Next = l2
			l2 = l2.Next
		}
		tail = tail.Next
	}
	if l1 != nil {
		tail.Next = l1
	} else {
		tail.Next = l2
	}
	return head.Next

}

func MergeTwoLists() *LinkedListNode {
	list1 := &LinkedListNode{}
	list2 := &LinkedListNode{}
	list1.insert(10)
	list1.insert(50)
	list1.insert(80)
	list2.insert(40)
	list2.insert(60)
	list2.insert(70)
	merged := list1.merge(list2)
	return merged
}

package solutions

type son struct {
	Sum   int
	Index int
}

func FindTarget() bool {
	n := []int{1, 2, 6, 9, 13, 15, 105, 25, 3, 4}
	memo := make(map[son]bool)
	return recSum(n, 0, 0, 100, memo)
}
func recSum(n []int, index, curSum, target int, memo map[son]bool) bool {
	if index < len(n) {
		so := son{curSum, index}
		if valu, ok := memo[so]; ok {
			return valu
		}
		pick := curSum + n[index]
		nonPick := curSum
		memo[so] = recSum(n, index+1, pick, target, memo) || recSum(n, index+1, nonPick, target, memo)
		return memo[so]
	} else {
		return curSum == target
	}
}

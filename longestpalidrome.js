/**
 * @param {string} s
 * @return {string}
 */
// remove the frist element call the check palindrome string and also remove last element and call the check palindrome
var longestPalindrome = function (s) {
    let mem = {}
    palvalue = isPlaindrome(s, mem)
    console.log(palvalue)
    return palvalue
};

function isPlaindrome(s, mem) {
    if (s in mem) {
        return mem[s]
    }

    if (s.length == 1) return s
    i = 0
    j = s.length - 1
    while (i <= j && s[i] == s[j]) {
        if (i == j || i + 1 == j) {
            return s
        }
        i++;
        j--;
    }

    if (s.length > 1) {
        let palidrome = isPlaindrome(s.substring(0, s.length - 1), mem)
        let pal = isPlaindrome(s.substring(1, s.length), mem)

        mem[s] = pal.length > palidrome.length ? pal : palidrome
        return pal.length > palidrome.length ? pal : palidrome
    }
    return s
}
longestPalindrome("zudfweormatjycujjirzjpyrmaxurectxrtqedmmgergwdvjmjtstdhcihacqnothgttgqfywcpgnuvwglvfiuxteopoyizgehkwuvvkqxbnufkcbodlhdmbqyghkojrgokpwdhtdrwmvdegwycecrgjvuexlguayzcammupgeskrvpthrmwqaqsdcgycdupykppiyhwzwcplivjnnvwhqkkxildtyjltklcokcrgqnnwzzeuqioyahqpuskkpbxhvzvqyhlegmoviogzwuiqahiouhnecjwysmtarjjdjqdrkljawzasriouuiqkcwwqsxifbndjmyprdozhwaoibpqrthpcjphgsfbeqrqqoqiqqdicvybzxhklehzzapbvcyleljawowluqgxxwlrymzojshlwkmzwpixgfjljkmwdtjeabgyrpbqyyykmoaqdambpkyyvukalbrzoyoufjqeftniddsfqnilxlplselqatdgjziphvrbokofvuerpsvqmzakbyzxtxvyanvjpfyvyiivqusfrsufjanmfibgrkwtiuoykiavpbqeyfsuteuxxjiyxvlvgmehycdvxdorpepmsinvmyzeqeiikajopqedyopirmhymozernxzaueljjrhcsofwyddkpnvcvzixdjknikyhzmstvbducjcoyoeoaqruuewclzqqqxzpgykrkygxnmlsrjudoaejxkipkgmcoqtxhelvsizgdwdyjwuumazxfstoaxeqqxoqezakdqjwpkrbldpcbbxexquqrznavcrprnydufsidakvrpuzgfisdxreldbqfizngtrilnbqboxwmwienlkmmiuifrvytukcqcpeqdwwucymgvyrektsnfijdcdoawbcwkkjkqwzffnuqituihjaklvthulmcjrhqcyzvekzqlxgddjoir")